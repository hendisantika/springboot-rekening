# Spring Boot Rekening

### Things todo list:

1. Clone this repository: `git clone https://gitlab.com/hendisantika/springboot-rekening.git`
2. Navigate to the folder: `cd springboot-rekening`
3. Run the application: `mvn clean spring-boot:run`
4. Open POSTMAN Collection

### Images Screen shot

Create New Rekening

![Create New Rekening](img/create.png "Create New Rekening")

List All Rekening

![List All Rekening](img/list.png "List All Rekening")

Find Rekening by NoRek

![Find Rekening by NoRek](img/find.png "Find Rekening by NoRek")

Transfer money

![Transfer money](img/transfer.png "Transfer money")
