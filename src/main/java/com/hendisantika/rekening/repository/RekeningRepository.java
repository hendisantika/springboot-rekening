package com.hendisantika.rekening.repository;

import com.hendisantika.rekening.entity.Rekening;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-rekening
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 03/06/21
 * Time: 08.34
 */
public interface RekeningRepository extends CrudRepository<Rekening, Long> {

    Rekening findByNorek(String norek);
}
