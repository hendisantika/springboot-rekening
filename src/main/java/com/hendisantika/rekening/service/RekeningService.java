package com.hendisantika.rekening.service;

import com.hendisantika.rekening.entity.Rekening;
import com.hendisantika.rekening.repository.RekeningRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-rekening
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 03/06/21
 * Time: 08.35
 */
@Service
public class RekeningService {

    @Autowired
    private RekeningRepository rekeningRepository;

    public Rekening create(Rekening rekening) {
        return rekeningRepository.save(rekening);
    }

    public Rekening findByNorek(String norek) {
        return rekeningRepository.findByNorek(norek);
    }

    public Iterable<Rekening> findAll() {
        return rekeningRepository.findAll();
    }

    @Transactional
    public void transfer(String norek1, String norek2, double amount) {

        Rekening rekening1 = rekeningRepository.findByNorek(norek1);

        if (rekening1 == null) {
            throw new RuntimeException("Norek1 tidak valid");
        }
        if (rekening1.getSaldo() < amount) {
            throw new RuntimeException("Saldo tidak cukup");
        }
        rekening1.setSaldo(rekening1.getSaldo() - amount);
        rekeningRepository.save(rekening1);

        Rekening rekening2 = rekeningRepository.findByNorek(norek2);
        if (rekening2 == null) {
            throw new RuntimeException("Norek2 tidak valid");
        }
        rekening2.setSaldo(rekening2.getSaldo() + amount);
        rekeningRepository.save(rekening2);

    }
}
