package com.hendisantika.rekening;

import com.hendisantika.rekening.entity.Rekening;
import com.hendisantika.rekening.repository.RekeningRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class SpringbootRekeningApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootRekeningApplication.class, args);
    }

    @Bean
    public CommandLineRunner demo(RekeningRepository rekeningRepository) {
        return (args) -> {
            rekeningRepository.save(new Rekening(1L, "ABC1001", "Uzumaki Naruto", 10000.0));
            rekeningRepository.save(new Rekening(2L, "ABC1002", "Uchiha Sasuke", 20000.0));
            rekeningRepository.save(new Rekening(3L, "ABC1003", "Haruno Sakura", 30000.0));
            rekeningRepository.save(new Rekening(4L, "ABC1004", "Hatake Kakashi", 40000.0));
            rekeningRepository.save(new Rekening(5L, "ABC1005", "Jiraiya", 50000.0));
        };
    }

}
