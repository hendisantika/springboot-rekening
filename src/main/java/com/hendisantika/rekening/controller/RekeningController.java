package com.hendisantika.rekening.controller;

import com.hendisantika.rekening.dto.TransferRequest;
import com.hendisantika.rekening.entity.Rekening;
import com.hendisantika.rekening.service.RekeningService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-rekening
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 03/06/21
 * Time: 08.52
 */
@RestController
@RequestMapping("/rekening")
public class RekeningController {

    @Autowired
    private RekeningService rekeningService;

    @PostMapping
    public Rekening create(@RequestBody Rekening rekening) {
        return rekeningService.create(rekening);
    }

    @GetMapping
    public Iterable<Rekening> findAll() {
        return rekeningService.findAll();
    }

    @GetMapping("/{norek}")
    public Rekening findByNoRek(@PathVariable("norek") String norek) {
        return rekeningService.findByNorek(norek);
    }

    @PostMapping("/transfer")
    public void transfer(@RequestBody TransferRequest transfer) {
        rekeningService.transfer(transfer.getNorek1(), transfer.getNorek2(), transfer.getAmount());
    }

}
