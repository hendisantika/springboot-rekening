package com.hendisantika.rekening.dto;

import lombok.Data;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-rekening
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 03/06/21
 * Time: 08.33
 */
@Data
public class TransferRequest {
    private String norek1;
    private String norek2;
    private double amount;
}
